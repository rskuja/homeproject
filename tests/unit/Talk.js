
/**
 *  Talks Unit Test
 *  Created by create-test script @ Sat Oct 29 2011 17:35:05 GMT+0300 (EEST)
 **/
/**
 * Dependencies
 */
var     should = require('should')
	  , mongoose = require('mongoose')
	  , example = require('models/Talk')
	  , Schema = mongoose.Schema
	  , SchemaType = mongoose.SchemaType
	  , ValidatorError = SchemaType.ValidatorError
	  , DocumentObjectId = mongoose.Types.ObjectId
	  , MongooseError = mongoose.Error;

/**
 * Simple expresso tests for the Talk model
 */
module.exports = {
		    
  'Test that a model can be created': function(){
	    var Talk = mongoose.model('Talk');
	    var model = new Talk();
	    model.isNew.should.be.true;    
   },
  'Test that the model is an instance of a mongoose schema': function(){
    var Talk = mongoose.model('Talk');
    Talk.schema.should.be.an.instanceof(Schema);
    Talk.prototype.schema.should.be.an.instanceof(Schema);
  },
  'Test that an Talk has all of the default fields and values': function(){
    
    var Talk = mongoose.model('Talk');

    var model = new Talk();
    model.isNew.should.be.true;

    model.get('_id').should.be.an.instanceof(DocumentObjectId);
    should.equal(undefined, model.get('name'));
    
   },
  'Test that saving a record with invalid fields returns a validation error': function(){
	  
	    var Talk = mongoose.model('Talk');	    
	    var model = new Talk();
	    model.set('name', '');
	    model.save(function(err){
	      
	      err.should.be.an.instanceof(MongooseError);
	      err.should.be.an.instanceof(ValidatorError);
	      
	      model.set('name', 'I exist!');
	      model.save(function(err){
	        should.strictEqual(err, null);
	      });
	      
	    });	    

  }

};