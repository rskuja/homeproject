/**
 * Module dependencies.
 */
var cluster = require('cluster');
var app;

/**
 * Initial bootstrapping
 */
exports.boot = function(port,path){
  
  //Create our express instance	
  app = require('./app').boot();
  /*
var watchFolders = [path + '/models', 
                      path + '/controllers',
                      path + '/views',
                      path + '/utils']*/
  
  cluster(app)
  	  .set('working directory', path)
  	  .set('socket path',path)
	  .in('development')
	    .set('workers', 2)	    
	    .use(cluster.logger(path + '/logs', 'debug'))	   
	    .use(cluster.pidfiles(path + '/pids'))
	    //.use(cluster.reload(watchFolders,{ signal: 'SIGTERM', extensions: ['.js'] }))
	  .in('test')
	    .set('workers', 1)
	    .use(cluster.logger(path + '/logs', 'warning'))	    
	    .use(cluster.pidfiles(path + '/pids'))
	  .in('production')
	    .set('workers', 2)
		.use(cluster.logger(path + '/logs'))	    
		.use(cluster.pidfiles(path + '/pids'))
	  .in('all')
	    .listen(parseInt(80));
	  
};
