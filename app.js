/**
 * Module dependencies.
 */
var fs = require('fs'),express = require('express');
var mongoose = require('mongoose');
var mongooseAuth = require('mongoose-auth');
var auth = require('./models/User');
var form = require('connect-form');
var path = __dirname;
var app;
var mongoStore = require('connect-mongo');
 
/**
 * Initial bootstrapping
 */
exports.boot = function(params){

  //Create our express instance
  app = express.createServer(
    form({ keepExtensions: true })
  );
   // Import configuration
  require(path + '/conf/configuration.js')(app,express);
  
  // Bootstrap application
  bootApplication(app);
  bootModels(app);
  bootControllers(app);
  mongooseAuth.helpExpress(app);

  // Example 404 page via simple Connect middleware
  app.use(function(req, res){
    res.render('404');
  });
  return app;
  
};

/**
 *  App settings and middleware
 *  Any of these can be added into the by environment configuration files to 
 *  enable modification by env.
 */

function bootApplication(app) {	 
   
   // launch
  // app.use(express.logger({ format: ':method :url :status' }));
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(express.cookieParser());
  //app.use(express.session({ secret: 'helloworld' }));
  app.use(express.session({
    secret: 'topsecret',
    maxAge: new Date(Date.now() + 3600000),
    store: new mongoStore({ host: 'localhost', port: 27017, db: 'seesion', collection: 'sessions' })
  }));
  app.use(mongooseAuth.middleware());
  app.use(express.static(path + '/public'));  // Before router to enable dynamic routing

  // Example 500 page
  app.error(function(err, req, res){
    res.render('500',{error:err});
  });
  
  

  // Setup ejs views as default, with .html as the extension
  app.set('views', path + '/views');
  app.register('.html', require('ejs'));
  app.set('view engine', 'html');

  
  // Some dynamic view helpers
  app.dynamicHelpers({
  
	request: function(req){
	   return req;
	},
	    
	hasMessages: function(req){
      return Object.keys(req.session.flash || {}).length;
    },

  activehouse: function(req){
      return req.session.activehouse;
  },
  activehouse_img: function(req){
      return req.session.activehouse_img;
  },
  activehouse_name: function(req){
      return req.session.activehouse_name;
  },

  messages: function(req){
    return function(){
      var msgs = req.flash();
      console.log(msgs);
      return Object.keys(msgs).reduce(function(arr, type){
        return arr.concat(msgs[type]);
      }, []);        
    }
  }
  });

}

//Bootstrap models 
function bootModels(app) {
	
  fs.readdir(path + '/models', function(err, files){
    if (err) throw err;
    files.forEach(function(file){
    	bootModel(app, file);
    });
  });
  
  // Connect to mongoose
  mongoose.connect(app.set('db-uri'));
}

// Bootstrap controllers
function bootControllers(app) {
  fs.readdir(path + '/controllers', function(err, files){
    if (err) throw err;
    files.forEach(function(file){    	
    	 bootController(app, file);    		
    });
  });
  
  require(path + '/controllers/AppController')(app);			// Include
}

// simplistic model support
function bootModel(app, file) {

    var name = file.replace('.js', ''),
    	schema = require(path + '/models/'+ name);				// Include the mongoose file        
    
}

// Load the controller, link to its view file from here
function bootController(app, file) {

	var name = file.replace('.js', ''),
    	controller = path + '/controllers/' + name,   // full controller to include
    	template = name.replace('Controller','').toLowerCase();									// template folder for html - remove the ...Controller part.
  // require(path + '/controllers/' + name)(app);
  // Include the controller
	  require(controller)(app);			// Include
	
}
