/**
 *  Users Controller
 *  Created by create-controller script @ Sat Oct 29 2011 17:35:05 GMT+0300 (EEST)
 **/
 var mongoose = require('mongoose'),	
	User = mongoose.model('User'),
	ViewTemplatePath = 'users';

module.exports = function(app) {
	app.get('/users/show/:id', show);
};

/**
 * Show action, returns shows a single item via views/talks/show.html view or via json
 * Default mapping to GET '/users/show/:id'
 **/	
function show(req, res) {	  		  
	
	  User.findById(req.params.id, function(err, user) {
	  
		  if(err) return next(err);
	  
	      switch (req.params.format) {
        case 'json':
          res.send(user.toObject());
          break;

        default:
        	res.render(ViewTemplatePath + "/show",{vuser:user});
      }
      
  });
      
}