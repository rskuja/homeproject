var fs = require('fs');

var	pager = require('../utils/pager.js'), 
	guide = require('../utils/guidegenerator.js'),
	inflection = require('../utils/inflection');

var TalkModel = require('../models/Talk'),
	Units = require('../models/Units');

var mongoose = require('mongoose'),	
	Talk = mongoose.model('Talk'),
	Comment = mongoose.model('Comments'),
	UserVote = mongoose.model('UserVote'),
	Units = mongoose.model('Units'),
	User = mongoose.model('User');

module.exports = function(app) {
	app.get('/', index);
	app.get('/getuser', getuser);
	app.get('/reg/:id', index);
	app.post('/server/upload', uploader);
	app.get('/logout', function (req, res) {
	  req.logout();
	  res.redirect('/');
	});

};

// GET: /getuser send JSON with current user data
// TODO: should put all in session
function getuser(req, res) {
	if(req.user)
	{ 
		User.findById(req.user._id)
		.populate('houses')
		.populate('invites')
		.populate('units')
		.run( function(err, user){
			if(err) res.send('false');
			res.send({ user: user.toObject(), house: req.session.activehouse});
		});
	}else
	{
		res.send('false');
	}
}
// GET "/" main index page 
// if not register renders startpage
// else renders wall
function index(req, res) {
	  	 
	  var from = req.params.from ? parseInt(req.params.from) - 1 : 0;
	  var to = req.params.to ? parseInt(req.params.to) : 10;
      var total = 0;
      if(req.user)
	  {
			if(req.session.activehouse == undefined || req.session.activehouse == ""){
				if(req.user.houses.length > 0){
				User.findById(req.user._id, function(err,usr)
				{
			      	req.session.activehouse =  usr.houses[0];
				});
				}else req.session.activehouse = "";
			}  
	        Talk.count({}, function (err, count) {
		    	total = count;  
		    	var pagerHtml = pager.render(from,to,total,'/talks');    	
			    Talk.find()
			  	.where('house').in(req.user.houses)
			  	.where('decision', undefined)
			  	.skip(from).limit(to)
			  	.populate('votes')
			  	.populate('house')
			  	.desc('date')
			  	.find(function (err, talks) {
			  			UserVote.find({ user: req.user._id},['vote'])
		  				.where('parent').in(talks)
		  				.run( function (err, uservotes){
			  			  switch (req.params.format) {
					        case 'json':	          
					          res.send(votes.map(function(u) {
					              return u.toObject();
					          }));
					          break;
				
					        default:			        	
					        	res.render("app",{talks:talks,pagerHtml:pagerHtml, myvotes: uservotes, unit:null});
					      }
					    });
			    });
	      
	      });
      }
	  else{
	  	if(req.session.activehouse != undefined){
	  		req.session.activehouse = undefined;
	  	}
	  	if(req.params.id != undefined){
	  		Units.findById(req.params.id)
	  		.populate('parent')
	  		.run(function(err, unit){
	  				if(err) res.render("app", {unit: null});
					res.render("app", {unit:unit});
				});  		
	  	}else
	  	{
	  		res.render("app", {unit: null});
	  	}
	  } 
      	  	
}

// file uploader 
// gets file extension from fileuploader.js
// client side fileuploader source: https://github.com/valums/file-uploader
// with my modifications for passing safe logical file group and extension
// valid file group must be set on client side or will be saved to tmp for security check
// uses guide generator for safe file names
// doublechecks file extension for safety
function uploader(req, res, next) {

	var dirgroup = {
		"housepic": "images/houses/",
		"talkpic": "images/talks/",
		"houseatt": "attachments/houses/",
		"talkatt": "attachments/talks/",
		"inboxpic": "images/inbox/",
		"inboxatt": "attachments/inbox/",
		} // add more directories when needed
   var allowed = ['png','gif','jpg', 'doc', 'docx', 'odt',
   				  'xls', 'xlsx','ods', 'ppt', 'pptx', 'odp',
   				   'pdf', 'txt','zip','rar'] // add more safe ext when needed
  if(req.xhr) {    
    var ext = req.header('x-file-ext')
      , dgroup = req.header('x-file-path')
  	  , rndname = guide.generate()+"."+ext
  	  , path = dirgroup[dgroup]
      , issafe = false;
    for (var i=0; i<allowed.length; i++){
        if (allowed[i].toLowerCase() == ext){ issafe = true;}    
    }
    if(issafe){
	    var ws = fs.createWriteStream(__dirname+'/../public/'+path+rndname);
	    req.on('data', function(data) {
	      ws.write(data); 
	    }); 
	    req.on('end', function(data) { 
	      var body = '{"success":"true", "name": "'+rndname+'"}';
	      res.writeHead(200, 
	        { 'Content-Type':'text/plain'
	        , 'Content-Length':body.length
	        });
	      res.end(body);
	    }); 
    }
    else {
    	var body = '{"success":"false"}'
    	res.writeHead(200, 
	        { 'Content-Type':'text/plain'
	        , 'Content-Length':body.length
	        });
	    res.end(body);
    }
  }  
    return;
}