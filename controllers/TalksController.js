
/**
 *  Talks Controller
 *  Created by create-controller script @ Sat Oct 29 2011 17:35:05 GMT+0300 (EEST)
 **/
 var mongoose = require('mongoose'),	
	Talk = mongoose.model('Talk'),
	UserVote = mongoose.model('UserVote'),
	Comment = mongoose.model('Comments'),
	Property = mongoose.model('Property'),
	pager = require('../utils/pager.js'),
	guide = require('../utils/guidegenerator.js'),
	ViewTemplatePath = 'talks';

module.exports = function(app) {
	app.get('/talks', index);
	app.get('/talks/create', create);
	app.post('/talks/add', addTalk);
	app.get('/talks/show/:id', show);
	app.del('/talks/destroy/:id', destroy);
	app.get('/talks/edit/:id', edit);
	app.post('/talks/update/:id',update);
	app.post('/talks/addcomment/:id', addComment);
};

	/**
	 * Index action, returns a list either via the views/talks/index.html view or via json
	 * Default mapping to GET '/talks'
	 * For JSON use '/talks.json'
	 **/
	function index(req, res) {
		  	 
		  var from = req.params.from ? parseInt(req.params.from) - 1 : 0;
		  var to = req.params.to ? parseInt(req.params.to) : 10;
	      var total = 0;
	      if(req.session.activehouse)
		  {
		  	Talk.count({}, function (err, count) {
	    	total = count;  
	    	var pagerHtml = pager.render(from,to,total,'/talks');    	
	                  
			  Talk.find( { $where : "this.votes.length == 0", 'house': req.session.activehouse } )
			  	.populate('house')
			  	.where('decision', undefined)
			  	.desc('date')
			  	.skip(from).limit(to)
			  	.find(function (err, talks) {
				  if(err) return next(err);
				  
			      switch (req.params.format) {
			        case 'json':	          
			          res.send(talks.map(function(u) {
			              return u.toObject();
			          }));
			          break;
			        default:			        	
			        	res.render(ViewTemplatePath,{talks:talks,pagerHtml:pagerHtml});
			      } 
			  });
	      
	        });
		  }else res.render(ViewTemplatePath,{talks: []});

	      	  	
	}

	// GET: /talks/create
	// sends to new talk creation page (static template)
	function create(req, res) {
		res.render(ViewTemplatePath + "/create");
	}
	
	/**
	 * Show action, returns shows a single item via views/talks/show.html view or via json
	 * Default mapping to GET '/talks/show/:id'
	 **/	
 	function show(req, res) {	  		  
			
 		  Talk.findById(req.params.id)
 		  .populate('house')
 		  .populate('decision')
 		  .populate('votes')
 		  .where('house').in(req.user.houses)
 		  .run(function(err, talk) {
 			  if(err) return next(err);
 			  if(talk != null)
 			  {
	  			UserVote.find({ user: req.user._id, parent: talk.id},['vote'])
	  			.run( function (err, uservotes){
 			  	switch (req.params.format) {
			        case 'json':
			          res.send(talk.toObject());
			          break;
		
			        default:
    	 			  	var isadmin = false;
		 			  	for(var i=0; i<talk.house.admins.length; i++){
		 			  		if(JSON.stringify(talk.house.admins[i]) === JSON.stringify(req.user._id)){
		 			  			isadmin = true;
		 			  			break;
		 			  		}
		 			  	}
			        	res.render(ViewTemplatePath + "/show",{talk:talk, isadmin: isadmin, myvotes: uservotes});
			      }
		      });
 			  }else res.render("noaccess");
		  });
		      
 	}
	
	/**
	 * Edit action, returns a form via views/talks/edit.html view no JSON view.
	 * Default mapping to GET '/talks/edit/:id'
	 **/  	  
	function edit(req, res){
		  Talk.findById(req.params.id)
		  .where('house').in(req.user.houses)
		  .run(function(err, talk) {
			  if(err) return next(err);
			  if(talk != null)
 			  {
			  res.render(ViewTemplatePath + "/edit",{talk:talk});
			  }else res.render("noaccess");
		});
	}
	  
	/**
	 * Update action, updates a single item and redirects to Show or returns the object as json
	 * Default mapping to PUT '/talks/update/:id', no GET mapping	 
	 **/  
	function update(req, res){
	    
	    Talk.findById(req.params.id)
	   	.where('house').in(req.user.houses)
	    .run(function(err, talk) {
	        if(talk != null)
			{
		    	talk.title = req.body.talk.title;
		    	talk.content = req.body.talk.content;
		    	
		        talk.save(function(err) {
		        
		    	  if (err) {
		    		  console.log(err);
		        	  req.flash('error','Neizdevās labot sarunu: ' + err);
		          	  res.redirect('/talks');
		          	  return;
		    	  }
		    		
		          switch (req.params.format) {
		            case 'json':
		              res.send(talk.toObject());
		              break;
		            default:
		              req.flash('info', 'Saruna izmainīta');
		              res.redirect('/talks/show/' + req.params.id);
		          }
		        });
	        }else res.render("noaccess");
	    });
	}
	  
	/**
	 * Create action, creates a single item and redirects to Show or returns the object as json
	 * Default mapping to POST '/talks/add', no GET mapping	 
	 **/  
	function addTalk(req, res){
		  
		  var talk = new Talk(req.body.talk);
		  talk.date = new Date();
		  talk.house = req.session.activehouse;
		  for(var i=1; i<req.body.pictures.length; i++)
		  {
		  	talk.pictures.push(req.body.pictures[i]);
		  }
		  for(var i=1; i<req.body.attachments.length; i++)
		  {
		  	talk.attachments.push(req.body.attachments[i]);
		  }
		  talk.save(function(err) {
		   
			if (err) {
	    	  req.flash('error','Neizdevās izveidot sarunu: ' + err);
	      	  res.redirect('/talks');
	      	  return;
			}
			Property.findById(req.session.activehouse, function(err,property){
			  	property.talks.push(talk.id);
			  	property.save();
			  });
		    switch (req.params.format) {
		      case 'json':
		        res.send(talk.toObject());
		        break;
	
		      default:
		    	  req.flash('info','Jauna saruna izveidota veiksmīgi');
		      	  res.redirect('/talks/show/' + talk.id);
			 }
		  });	  
		  
	}

	/**
	 * Create action, creates a single item and redirects to Show or returns the object as json
	 * Default mapping to POST '/talks/addcomment', no GET mapping	 
	 **/  
	function addComment(req, res){
		if(req.body.comment.content.length > 1){
		Talk.findById(req.params.id)
		.where('house').in(req.user.houses)
	    .run(function(err, talk) {
	    	if(talk != null)
			{
				var comment = new Comment(req.body.comment);
				comment.userid = req.user._id;
				comment.username = req.user.fullname;
				comment.date = new Date();
				talk.comments.push(comment);
				talk.save(function(err) {
			    	if (err) {
				    	console.log(err);
				        req.flash('error','Neizdevās pievienot komentāru: ' + err);
				        res.redirect('back');
				        return;
			    	}
		            switch (req.params.format) {
			            case 'json':
			              res.send(talk.toObject());
			              break;
			            default:
			              res.redirect('back');
		            }
		        });  
	        }else res.render("noaccess");
		  });
		}else res.redirect('back');
	}

	  
	/**
	 * Delete action, deletes a single item and redirects to index
	 * Default mapping to DEL '/talks/destroy/:id', no GET mapping	 
	 **/ 
	function destroy(req, res){
		  
		  Talk.findById(req.params.id)
		  .where('house').in(req.user.houses)
	      .run(function(err, talk) {
		        if(talk != null)
				{	    
			    	talk.remove(function(err) {
		    		  if(err) {
		    	    	  req.flash('error','Neizdevās izdzēst!');
		    			  res.send('false');
		    		  } else {
		    	    	  req.flash('info','Ieraksts izdzēsts veiksmīgi');
		    			  res.send('true');
		    		  }    	          
		   	      	});
		   	    }else res.render("noaccess");
		  });
		  
	}