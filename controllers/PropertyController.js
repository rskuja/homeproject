// *
//  *  Property Controller
//  *
  var mongoose = require('mongoose'),	
  	User = mongoose.model('User'),
	Property = mongoose.model('Property'),
	Units = mongoose.model('Units'),
	Talk = mongoose.model('Talk'),
	ViewTemplatePath = 'property',
	fs = require('fs'),
	util = require('util'),
	guide = require('../utils/guidegenerator.js'),
	User = mongoose.model('User'),
	Works = mongoose.model('Works'),
	Personal = mongoose.model('Personal'),
	ParkingPlace = mongoose.model('ParkingPlace'),
	PropertyDecision = mongoose.model('PropertyDecision'),
	Meaters = mongoose.model('Meaters'),
	emailer = require('../utils/emailer.js');

var formidable = require('formidable'),
    http = require('http'),
    sys = require('sys');

module.exports = function(app) {
	app.get('/property/new', newproperty);
	app.post('/property/add', add);
	app.get('/property/edit/:id', edit);
	app.post('/property/addimage', addimage);
	app.post('/property/update/:id', update);
	app.get('/property/switch/:id',switchactive);
	app.get('/property/units/:id', units);
	app.post('/property/update_units/:id', update_units);
	app.get('/houseinfo/:id', houseinfo);
	app.get('/property/confirmunit/:id', confirmunit);
	app.get('/property/unit/:id', unit);
	app.get('/property/show/:id', show);
	app.post('/property/make_decision/:id', addDecision);
	app.get('/property/personal/:id', personal);
	app.post('/property/addwork/:id', addwork);
	app.post('/property/update-personal/:id', updatePersonal);
	app.post('/property/addparking/:id', addparking);
	app.get('/property/decision-history/:id', showDecisionHistory);
	app.get('/property/planned-works/:id', showPlannedWorks);
	app.get('/property/parking/:id', showParking);
	app.get('/property/meaters/:id', meaters);
	app.get('/property/unitmeaters/:id', unitmeaters);
	app.post('/property/setmeaters/:id', setmeaters);
	// app.get('/property/incomes/:id'), incomes);
	// app.get('/property/expenses/:id'), expenses);
	// app.get('/property/maintenance/:id'), maintenance);
	// app.get('/property/provisions/:id'), provisions);
};

// GET house info with :id , used for json
function houseinfo(req, res) {
	Property.findById(req.params.id)
	.populate('units')
	.run( function(err, property){
		res.send(property.toObject());
	});
}

// house invite confirmation
// GET /property/confirmunit/:id
// get active user, remove unit from id, push in units and houses.
function confirmunit(req, res) {
	User.findById(req.user._id)
	.populate('invites')
	.run( function(err, user){
		var invcount = user.invites.length;
		var confirmed = false;
		for(var i=0; i<invcount; i++)
		{
			if(user.invites[i].id == req.params.id)
			{
				user.houses.push(user.invites[i].parent);
				user.units.push(user.invites[i].id);
				user.invites.remove(req.params.id);
				user.save();
				Units.findById(user.invites[i].id, function(err, unit){
					unit.owner = user.id;
					unit.save();
				});
				confirmed = true;
				break;
			}
		}
		if(confirmed) res.send('true');
		else res.send('false');
	});
}

// GET /property/new 
function newproperty(req, res) { res.render(ViewTemplatePath + "/new", {image: null}); }

// Switch active house 
// GET /property/switch/:id
// stores in session new active house id (req.sesssion.activehouse)
function switchactive(req, res) {
	var housecount = req.user.houses.length;
	for(var i=0; i<housecount; i++)
	{
		if(req.params.id == req.user.houses[i]) req.session.activehouse = req.params.id;
	}
	res.redirect('back');
}
// *
//  * Default mapping to POST '/property/add', no GET mapping	 
//  *  
function add(req, res){
	  
	  if(req.user)
	  {
	  	var property = new Property(req.body.property);
	  	property.country = "Latvija";
	  	property.creation_date = new Date();
	  	property.admins.push(req.user._id);
	  	property.save(function(err) {
	  		if(err) console.log(err);
	  		User.findById(req.user._id, function(err, user) {
	  			user.houses.push(property.id);
	  			user.save(function(err) {
	  				if(err) console.log(err);
	  				req.flash('info','Jauns īpašums izveidots veiksmīgi')
	  				res.redirect("/");
	  			});
	  		});
	  	});
	  }else
	  {
	  	req.flash('error','Neizdevās pievienot īpašumu!');
      	res.redirect(ViewTemplatePath + "/new");
	  }  
}

// GET: /property/edit/:id
// Renders house edit view as editable if admin(owner)
// Renders house edit view as uneditable info otherwise
function edit(req, res) {
	Property.findById(req.params.id)
	.populate('admins')
	.populate('units')
	.run( function(err, property) {
		  var admincount = property.admins.length;
		  var access = "false"
		  for(var i=0; i < admincount; i++) if(property.admins[i].id == req.user._id) {  access = "admin"; break; }
		  if(access == "admin"){
		  	res.render(ViewTemplatePath + "/edit",{property:property, edit: true});
		  } 
		  else {
		  	var unitcount = property.units.length;
		  	for(var i=0; i < unitcount; i++) if(property.units[i].owner == req.user.id) { access = "guest"; break; }
		  	if(access == "guest"){ res.render(ViewTemplatePath + "/edit",{property:property, edit: false}); }
		  	else res.render('noaccess');
		 }
	});
}

// GET /property/unit/:id
// Renders specific house unit info
function unit(req, res) {
	Units.findById(req.params.id)
	.populate('parent')
	.run(function(err, unit){
		if(req.user.email == unit.email)
		{
			res.render(ViewTemplatePath + "/unit",{unit:unit});
		}
		else res.render('noaccess');
	});
} 

// process for adding image
// TODO: need to improve this in many ways
function addimage(req, res, next){
  // connect-form adds the req.form object
  // we can (optionally) define onComplete, passing
  // the exception (if any) fields parsed, and files parsed
  req.form.complete(function(err, fields, files){
    if (err) {
      next(err);
    } else {
      console.log('\nuploaded %s to %s'
        ,  files.image.filename
        , files.image.path);
      ins = fs.createReadStream(files.image.path);
      ous = fs.createWriteStream('./public/images/' + files.image.filename);
      util.pump(ins, ous, function(err) {
        if(err) {
          res.send('false');
        } else {
          res.send('true');
        }
      });
      
    }
  });

  // We can add listeners for several form
  // events such as "progress"
  req.form.on('progress', function(bytesReceived, bytesExpected){
    var percent = (bytesReceived / bytesExpected * 100) | 0;
    process.stdout.write('Uploading: %' + percent + '\r');
  });
}


// *
//  * Default mapping to POST '/property/update/:id', no GET mapping	 
//  *  
function update(req, res){
	  if(req.user)
	  {
	      Property.findById(req.params.id, function(err, property) {
			  property.country = "Latvija";
			  property.name = req.body.property.name;
			  property.street = req.body.property.street;
			  property.homenr = req.body.property.homenr;
			  property.city = req.body.property.city;
			  property.region = req.body.property.region;
			  property.postal_code = req.body.property.postal_code;
			  property.unitcount = req.body.property.unitcount;
			  property.picture =req.body.property.picture;

			  property.save(function(err) {
				if (err) { console.log(err); }
				res.redirect("/property/edit/"+property.id);
			  });
		  });
	  }else
	  {
      	res.redirect(ViewTemplatePath + "/new");
	  }  
}

// GET: /property/units/:id
// renders list of property units (editable if admin).
function units(req,res) {
	Property.findById(req.params.id)
	.populate('admins')
	.populate('units')
	.run( function(err, property) {
		  var admincount = property.admins.length;
		  var access = false
		  for(var i=0; i < admincount; i++) if(property.admins[i].id == req.user._id) access = true;
		  if(access) res.render(ViewTemplatePath + "/units",{property:property});
		  else res.render('noaccess');
	});
}

// POST: /property/update_units/:id
// Updates data for units and sends invites to house
// TODO: needs some optimisation / clean up
function update_units(req,res) {
	Property.findById(req.params.id)
	.populate('units')
	.run(function(err, property){
		var unitcount = req.body.unit.count.length
		if(unitcount > 1)
		{
			for(var i=0; i< unitcount; i++){
			var pushin = true;
			for(var k=0; k < property.units.length; k++)
			{
				if(req.body.unit.id[i] != 0)
				{
					pushin = false;
					editunit(req.body.unit.id[i],
							 req.body.unit.title[i],
							 req.body.unit.name[i],
							 req.body.unit.surname[i],
							 req.body.unit.phone[i]);
					break;
				}
			}
			if(pushin)
			{
				var unit = new Units({
					title: req.body.unit.title[i],
					name: req.body.unit.name[i],
					surname: req.body.unit.surname[i],
					email: req.body.unit.email[i],
					phone: req.body.unit.phone[i],
					parent: property.id,
				});
				unit.save(function (err) {
					if (err) {
				    	req.flash('error','Neizdevās pievienot: ' + err);
	  	  				res.redirect('back');
				    } 
				});
				property.units.push(unit.id);
			    property.save(function (err) {
					if (err) {
						req.flash('error','Neizdevās pievienot: ' + err);
		  				res.redirect('back');
					}
				});
				if(req.body.unit.email[i] != req.user.email) sendmail(unit.id, unit.email, property.name);
				else {
					unit.owner = req.user._id;
					unit.save();
				}
			}

			}
		}
		else
		{
			var pushin = true;
			for(var k=0; k < property.units.length; k++)
			{
				if(req.body.unit.id != 0)
				{
					pushin = false;
					editunit(req.body.unit.id,
							 req.body.unit.title,
							 req.body.unit.name,
							 req.body.unit.surname,
							 req.body.unit.phone);
					break;
				}
			}
			if(pushin)
			{
				var unit = new Units({
					title: req.body.unit.title,
					name: req.body.unit.name,
					surname: req.body.unit.surname,
					email: req.body.unit.email,
					phone: req.body.unit.phone,
					parent: property.id,
				});
				unit.save(function (err) {
					if (err) {
				    	req.flash('error','Neizdevās pievienot: ' + err);
	  	  				res.redirect('back');
				    } 
				});
				property.units.push(unit.id);
			    property.save(function (err) {
					if (err) {
						req.flash('error','Neizdevās pievienot: ' + err);
		  				res.redirect('back');
					}
					if(req.body.unit.email != req.user.email) sendmail(unit.id, unit.email, property.name);
					else {
					unit.owner = req.user._id;
					unit.save();
					}
				});
				
			}
		}
		res.redirect('back');
		
	});
}

//Close vote pool if exists and adds decison on house profile
// POST /property/adddecision/:id
// Responds with json
function addDecision(req, res) {
	Property.findById(req.params.id, function(err, property){
		if(property != null){
			var access = false;
			for(var i=0; i < property.admins.length; i++ ){
				if(JSON.stringify(property.admins[i]) == JSON.stringify(req.user._id)){
					access = true;
					break;
				} 
			}
			if(access){
				var decision = new PropertyDecision(req.body.decision);
				decision.date = new Date();
				decision.manager = req.user._id;
				for(var i=1; i<req.body.attachments.length; i++)
				{
				  	decision.attachments.push(req.body.attachments[i]);
				}
				decision.save(function(err){
					var decId = decision._id;
					if(decision.ontalk != undefined) {
						Talk.findById(decision.ontalk, function(err,talk){
							talk.decision = decision._id;
							talk.save();
						});
					}
					property.decisions.push(decId);
					property.save(function(err){
						switch (req.params.format) {
				            case 'json':
				              res.send('true');
				              break;
				            default:
				              res.redirect('back');
				          }
					});
				});

			}
		}
		if(property === null || access === false)
		{
			switch (req.params.format) {
	            case 'json':
	              res.send('false');
	              break;
	            default:
	              res.redirect('/');
	          }
		}
	});	
}

// No GET/POST mapping
// sends invites to join portal & house or just house if all ready registred.
// sends new unit link to specified email and includes house name in body.
function sendmail(unit, email, house) {
	User.findOne({'email': email}, function(err, user){
		if(user != null){
			user.invites.push(unit);
			user.save();
			emailer.houseinvite(user.fullname, email, house);
		}else
		{
			emailer.sendinvite(unit,email,house,unit);
		}
	});
}

// GET /property/show/:id
function show(req, res) {
	Property.findById(req.params.id)
	.populate('plannedworks')
	.populate('units')
	.run( function(err, property){
		PropertyDecision.find({})
		.where('_id').in(property.decisions)
		.desc('date')
	  	.skip(0).limit(3)
		.populate('ontalk')
		.populate('manager')
		.run( function(err, decisions){
			res.render(ViewTemplatePath + "/show",{property: property,
			 decisions: decisions, isadmin: isadmin(property,req.user)});
		});
	});
}

// No GET/POST mapping
// edits specific unit with params
// gets unit id, title, name, surname and phone.
function editunit(id, title, name, surname, phone) {
	Units.findById(id)
	.run( function(err, uni)
	{
		uni.title = title;
		uni.name = name;
		uni.surname = surname;
		uni.phone = phone;
		uni.save(function(err){
			if(err) console.log(err);
		});
	});
}

function personal(req, res){
	Property.findById(req.params.id, function(err,property){
	  	if(isadmin(property,req.user)) res.render(ViewTemplatePath + "/personal",{property:property});
	  	else res.render('noaccess');
	})
}

function isadmin(property, user)
{
	var isadmin = false;
  	for(var i=0; i<property.admins.length; i++){
  		if(JSON.stringify(property.admins[i]) === JSON.stringify(user._id)){
  			isadmin = true;
  			break;
  		}
  	}
  	return isadmin;
}

function addwork(req, res){
	Property.findById(req.params.id, function(err, property){
		if(isadmin(property,req.user))
		{
			var work = new Works();
			work.date = new Date();
			work.comment = req.body.comment;
			work.property = req.params.id;
			work.creator = req.user._id
			work.save();
			property.plannedworks.push(work);
			property.save();
			res.redirect('back');
		}else res.render('noaccess');
	});
}

function addparking(req, res){
	parking = new ParkingPlace();
	parking.title = req.body.title;
	parking.unit = req.body.punit;
	Property.findById(req.params.id, function(err, property){
		if(isadmin(property,req.user)){
			property.parkings.push(parking);
			property.save();
			res.redirect('back');
		}
	});
}

function updatePersonal(req, res){
	Property.findById(req.params.id, function(err, property){
		if(isadmin(property,req.user)){
			property.rulles = req.body.rulles;
			property.personal = new Array();
			property.save(function(err){
				for(var i=1; i < req.body.fullname.length; i++)
				{
					if(req.body.title[i].length > 2 && req.body.fullname[i].length > 2){
						var personal = new Personal();
						personal.title = req.body.title[i];
						personal.fullname = req.body.fullname[i];
						personal.email = req.body.email[i];
						personal.phone = req.body.phone[i];
						property.personal.push(personal);
						property.save();
					}
				}
			});
			res.redirect('back');

		}else res.render('noaccess');
	});
}

function showDecisionHistory(req, res){
	Property.findById(req.params.id)
	.run( function(err, property){
		PropertyDecision.find({})
		.where('_id').in(property.decisions)
		.populate('ontalk')
		.populate('manager')
		.run( function(err, decisions){
			res.render(ViewTemplatePath + "/decision-history",{property: property,
			 decisions: decisions, isadmin: isadmin(property,req.user)});
		});
	});
}

function showPlannedWorks(req, res){
	Property.findById(req.params.id)
	.populate('plannedworks')
	.run( function(err, property){
			res.render(ViewTemplatePath + "/work-plans",{property: property, isadmin: isadmin(property,req.user)});
	});
}

function showParking(req, res){
	Property.findById(req.params.id)
	.populate('units')
	.run( function(err, property){
			res.render(ViewTemplatePath + "/parking",{property: property, isadmin: isadmin(property,req.user)});
	});
}

function meaters(req, res){
	Units.find( { 'parent': req.params.id } )
	.populate('meaters')
	.asc('_id')
	.run( function(err,units) {
		Property.findById(req.params.id)
		.populate('admins')
		.run( function(err, property) {
			  var admincount = property.admins.length;
			  var access = false
			  for(var i=0; i < admincount; i++) if(property.admins[i].id == req.user._id) access = true;
			  if(access) res.render(ViewTemplatePath + "/meaters",{units:units, property:property});
			  else res.render('noaccess');
		});
	});
}
// JSON info about unit meaters
function unitmeaters(req,res) {
	Units.findById(req.params.id)
	.populate('meaters')
	.run( function(err, unit){
		res.send(unit.toObject());
	});
}

function setmeaters(req,res){
	Units.findById(req.params.id)
	.run(function(err, unit){
		if(req.body.mid instanceof Array)
		{
			for (var i=1; i<req.body.mid.length; i++)
			{
				if(req.body.mid[i] == "new")
				{
					var meater = addMeater(req.body.hot[i],req.body.cold[i],req.body.heat[i],req.params.id);
					unit.meaters.push(meater.id);
					unit.save();
				}
				else
				{
					editMeater(req.body.mid[i],req.body.hot[i],req.body.cold[i],req.body.heat[i]);
				}
			}
		}
		// else
		// {
		// 	if(req.body.mid == "new")
		// 	{
		// 		var meater = addMeater(req.body.hot,req.body.cold,req.body.heat,req.params.id);
		// 		unit.meaters.push(meater.id);
		// 		unit.save();
		// 	}
		// 	else
		// 	{
		// 		editMeater(req.body.mid,req.body.hot,req.body.cold,req.body.heat);
		// 	}
		// }
		res.redirect('back');
	});
}

function addMeater(hot,cold,heat,unitId)
{
	var meaters = new Meaters();
	meaters.unit = unitId
	meaters.date = new Date();
	meaters.hotwater = hot;
	meaters.coldwater = cold;
	meaters.heat = heat;
	meaters.save();
	return meaters;
}

function editMeater(meaterId,hot,cold,heat)
{
	Meaters.findById(meaterId)
	.run(function(err,meater){
		meater.hotwater = hot;
		meater.coldwater = cold;
		meater.heat = heat;
		meater.save();
	});
}