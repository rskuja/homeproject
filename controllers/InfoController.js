ViewTemplatePath = 'info';


//Static info page routes
//Viewable without login
module.exports = function(app) {
	
	app.get('/info/about', function (req, res) {
		res.render(ViewTemplatePath + "/about")
	});

	app.get('/info/contacts', function (req, res) {
		res.render(ViewTemplatePath + "/contacts")
	});

	app.get('/info/terms', function (req, res) {
		res.render(ViewTemplatePath + "/terms")
	});

	app.get('/info/privacy', function (req, res) {
		res.render(ViewTemplatePath + "/privacy")
	});

	app.get('/info/faq', function (req, res) {
		res.render(ViewTemplatePath + "/faq")
	});
};

	