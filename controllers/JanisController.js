ViewTemplatePath = 'janis';

// Janis Melkis playground
// static pages for new design testing
module.exports = function(app) {
	
	app.get('/janis/1', function (req, res) {
		res.render("janis/1")
	});

	app.get('/janis/2', function (req, res) {
		res.render("janis/2")
	});
	app.get('/janis/3', function (req, res) {
		res.render("janis/3")
	});
	app.get('/janis/4', function (req, res) {
		res.render("janis/4")
	});
	app.get('/janis/5', function (req, res) {
		res.render("janis/5")
	});

	app.get('/janis/inbox', function (req, res) {
		res.render("janis/inbox")
	});
};

	