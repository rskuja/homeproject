 var mongoose = require('mongoose'),	
	pager = require('../utils/pager.js'),
	Inbox = mongoose.model('Inbox'),
	User = mongoose.model('User'),
	Units = mongoose.model('Units'),
	emailer = require('../utils/emailer.js'),
	Property = mongoose.model('Property'),
	ViewTemplatePath = 'inbox';




module.exports = function(app) {
	app.get('/inbox', inbox);
	app.get('/inbox/mult/:ids', inbox);
	app.post('/inbox/send', send);
	app.get('/inbox/sent', sent);
	app.get('/inbox/received', received);
	app.get('/inbox/show/:id', show);
	app.post('/inbox/replay/:id',replay);
};

// GET: /inbox
// displays inbox with contactlist to add
// no outsider mails
var tonames = [];
function inbox(req, res) {
	Property.find({})
	.where('_id').in(req.user.houses)
	.populate('admins')
	.populate('units')
	.run(function(err, houses){
		var ids = req.params.ids == null ? null : req.params.ids;
		if(ids != null)
		{	
			ids = ids.split(",");
			ids.shift();
			User.find()
			.where('_id').in(ids)
			.find(function(err, users){
				if(err) console.log(err);
				console.log(users);
				res.render(ViewTemplatePath, {houses: houses, ids: users});
			});
		}
		else res.render(ViewTemplatePath, {houses: houses, ids: []});
	});
}
// POST: /inbox/send
// internal mailservice
function send(req, res) {
	var mail = new Inbox();
    mail.date = new Date();
    mail.from = req.user._id;
    mail.subject = req.body.inbox.subject;
    mail.body = req.body.inbox.body;
    for(var i=1; i<req.body.pictures.length; i++)
	{
		mail.pictures.push(req.body.pictures[i]);
	}
 	var toids = req.body.toid;
 	// for each recipient
    for(var i=1; i < toids.length; i++)
    {
    	//stores to fields in mail
    	mail.to.push(toids[i]);
    	mail.save();

    	//gets recipient and adds mail to inbox
		User.findById(toids[i], function(err, user){
			user.inbox.push(mail.id);
			user.save();
			emailer.message(user.fullname, user.email, mail.id, req.user.fullname, mail.subject, mail.body, mail.pictures);
		});

	}
	res.redirect("/");
}

function sent(req, res) {
	Inbox.find({'from': req.user._id})
	.populate('from')
	.populate('to')
	.desc('date')
	.run(function(err, sent){
		res.render(ViewTemplatePath + "/sent", {sent: sent});
	});
}
function show(req, res) {
	Inbox.findById(req.params.id)
	.populate('from')
	.populate('to')
	.run(function(err, mail){
		User.findById(req.user._id, function(err, user){
			user.inbox.remove(mail.id);
			user.save();
		});
		res.render(ViewTemplatePath + "/show", {mail: mail});
	});
}

function received(req, res) {
	Inbox.find({ to : req.user._id })
	.populate('from')
	.desc('date')
	.run(function(err, inbox){
		res.render(ViewTemplatePath + "/inbox", {inbox: inbox});
	});
}

function replay(req, res) {
	Inbox.findById(req.params.id, function(err, mail){
		var newmail = new Inbox();
	    newmail.date = new Date();
	    newmail.from = req.user._id;
	    newmail.subject = "Re: " + mail.subject;
	    newmail.body = req.body.replay + "<blockquote>" + mail.body + "</blockquote>";
	 //    newmail.to.push(mail.from);
	 //    for(var i=1; i<req.body.pictures.length; i++)
		// {
		// 	newmail.pictures.push(req.body.pictures[i]);
		// }
	    newmail.save();
	    User.findById(mail.from, function(err, user){
			user.inbox.push(newmail.id);
			user.save();
			emailer.message(user.fullname, user.email, newmail.id, req.user.fullname, newmail.subject, newmail.body, newmail.pictures);
		});
	    res.redirect("/inbox/sent");
	});
}
