
 var mongoose = require('mongoose'),	
	Talk = mongoose.model('Talk'),
	VoteOption = mongoose.model('VoteOption'),
	Comment = mongoose.model('Comments'),
	pager = require('../utils/pager.js'),
	ViewTemplatePath = 'votes',
	UserVote = mongoose.model('UserVote'),
	Property = mongoose.model('Property');

module.exports = function(app) {
	app.get('/votes', index);
	app.get('/votes/create', create);
	app.post('/votes/add', addVote);
	app.post('/votes/makevote/:id', makeVote);
};


	/**
	 * Index action, returns a list either via the views/votes/index.html view or via json
	 * Default mapping to GET '/votes'
	 * For JSON use '/votes.json'
	 **/
	function index(req, res) {
		  	 
		  var from = req.params.from ? parseInt(req.params.from) - 1 : 0;
		  var to = req.params.to ? parseInt(req.params.to) : 10;
	      var total = 0;
	      
	      Talk.count({}, function (err, count) {
	    	total = count;  
	    	var pagerHtml = pager.render(from,to,total,'/votes');    	
	                  				
			  Talk.find( { $where : "this.votes.length > 0 ", 'house': req.session.activehouse } )
			  	.where('decision', undefined)
			  	.where('house').in(req.user.houses)
			  	.sort('date', -1)
			  	.populate('votes')
			  	.populate('house')
			  	.populate('UserVote')
			  	.skip(from).limit(to)
			  	.find(function (err, votes) {
			  	
			  		UserVote.find({ user: req.user._id},['vote'])
			  				.where('parent').in(votes)
			  				.run( function (err, uservotes){
			  			
			  			  switch (req.params.format) {
					        case 'json':	          
					          res.send(votes.map(function(u) {
					              return u.toObject();
					          }));
					          break;
				
					        default:			        	
					        	res.render(ViewTemplatePath,{votes:votes,pagerHtml:pagerHtml, myvotes: uservotes});
					      }
			  		});

			      
			  });
	      
	      });	  	
	}

	function create(req, res) {
		res.render(ViewTemplatePath + "/create");
	}

	/**
	 * Create action, creates a single item and redirects to Show or returns the object as json
	 * Default mapping to POST '/votes/add', no GET mapping	 
	 **/  
	function addVote(req, res){
		var talk = new Talk(req.body.vote);
		talk.date = new Date();
		talk.house = req.session.activehouse;
		for(var i=1; i<req.body.pictures.length; i++)
		{
			talk.pictures.push(req.body.pictures[i]);
		}
		for(var i=1; i<req.body.attachments.length; i++)
		  {
		  	talk.attachments.push(req.body.attachments[i]);
		  }
		talk.save(function(err) {
			if (err) {
			  req.flash('error','Neizdevās izveidot balsojumu: ' + err);
			  res.redirect('/votes');
			  return;
			}else
			{
			  Property.findById(req.session.activehouse, function(err,property){
			  	property.talks.push(talk.id);
			  	property.save();
			  });
			}
		});
  		req.body.voteoption.option.forEach(function(opt){
  			
			var vote = new VoteOption({
			      option: opt,
			      parent: talk._id,
			      count: 0,
			      allowmultiple: req.body.voteoption.multi == undefined ? false : true,
			      allowchange: req.body.voteoption.nochange == undefined ? true : false
			});

			vote.save(function (err) {
			    if (err) {
			    	req.flash('error','Neizdevās izveidot balsojumu: ' + err);
  	  				res.redirect('/votes');
			    }
			    talk.votes.push(vote._id);
			    talk.save();
			    console.log(talk.votes);
			});
		});	  
		req.flash('info','Jauns balsojums izveidots veiksmīgi');
		res.redirect('/talks/show/' + talk.id);  
	}

	function makeVote(req,res){
			Talk.findById(req.params.id)
			.where('house').in(req.user.houses)
			.run(function(err, talk){
				if(talk != null){		
					VoteOption.findById(req.body.vote, function(err, newvote){
						if(newvote.allowmultiple == false)
						{
							UserVote.findOne({parent: req.params.id, user: req.body.usr}, function(err, uservote){
								if(uservote == null)
								{	
									vote = new UserVote({
										parent: req.params.id,
										vote: req.body.vote,
										user: req.body.usr
									})
									vote.save(function (err){
										if(err) res.send('false');
										else {
												talk.uservotes.push(vote.id);
												talk.save();
												newvote.count++;
												newvote.save(function (err){
													if(err) res.send('false');
													else res.send('true');
												});
										}
									});
								}else
								{
									if(uservote.vote != req.body.vote)
									{			
											if(newvote.allowchange == false)
											{
												req.flash('error','Atvaino, bet šim balsojumam nav iespējams mainīt izvēli')
												res.send('true');
											}
											else
											{
												VoteOption.findById(uservote.vote, function(err, prev){
													prev.count--;
													prev.save(function (err){
														if(err) res.send('false');
													});
													uservote.vote = req.body.vote;
													uservote.save(function (err){
														if(err) res.send('false');
														newvote.count++;
														newvote.save(function (err){
															if(err) res.send('false');
															else res.send('true');
														});
													});
													
												});
												
											}
									}
								}
								
							});
						}
						else
						{
							UserVote.findOne({parent: req.params.id, user: req.body.usr, vote: req.body.vote}, function(err, uservote){
								if(uservote == null)
								{	
									vote = new UserVote({
										parent: req.params.id,
										vote: req.body.vote,
										user: req.body.usr
									})
									vote.save(function (err){
										if(err) res.send('false');
										else {
											talk.uservotes.push(vote.id);
											talk.save();
											newvote.count++;
											newvote.save(function (err){
												if(err) res.send('false');
												else res.send('true');
											});
										}
									});
								}else
								{
									talk.uservotes.remove(uservote.id);
									talk.save();										
									uservote.remove();
									uservote.save(function (err){
										if(err) res.send('false');
										newvote.count--;
										newvote.save(function (err){
											if(err) res.send('false');
											else res.send('true');
										});
									})
									
								}
							});
						}
						
					});
				}else res.send("false");
		  	});
	}