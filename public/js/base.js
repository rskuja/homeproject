var lvMonths = ["Janvāris","Februāris","Marts","Aprīlis","Maijs","Jūnijs","Jūlijs","Augusts","Septembris","Oktobris","Novembris","Decembris"];

//makes date look good (just now/1 hour ago/ect)
function prettyLinks(){
  var links = $(".date").get();  
  for ( var i = 0; i < links.length; i++ ){
  	var date = getDate(links[i].innerHTML);
	date = prettyDate(date);
	      if ( date )
	        links[i].innerHTML = date;
  }
}

function prettyMonths(){
	var months = $(".lv-month").get();
	for ( var i = 0; i < months.length; i++){
		var month = months[i].innerHTML;
		months[i].innerHTML = lvMonths[month];
	}
}

// converts to usable datetime string for prettyLinks
function getDate(date){
	var date = new Date(date);
	var datestr = date.toLocaleDateString() + "T" + date.toLocaleTimeString() + "Z";
	return datestr;
}

// ajax vote confirmation
// TODO: need to make JSON style vote refresh
function makevote(vote, opt, usr)
{
	$.ajax({
		   type: "POST",
		   cache: false,
		   url: "/votes/makevote/" + vote,
		   data: "usr=" + usr + "&vote="+opt,	   
		   success: function(msg) {				   	  
			  window.location.reload(true);
		   },
		   error: function(msg) {					  
			  alert("Neizdevās nobalsot");
		   }
	});
}
// ajax house invite confirmation
function confirmhouse(id){
    $.ajax({
      type: "GET",
      cache: false,
      url: "/property/confirmunit/" + id,
      success: function(msg) {
        location.reload();
      },
      error: function(msg) {           
        alert("Ielūgumu neizdevās apstiprināt");
      }
    });
}

function addpersonal(){
	$("#personal-list").append("<tr><td><input type='text' class='span3' name='title'></td><td><input type='text' class='span3' name='fullname'></td><td><input type='text' class='span3' name='phone'></td><td><input type='text' class='span3' name='email'></td></tr>");
}

function addDocument(){
	var comment = $("#subject").val();
	var doc = $("#showatt").html();
	if(comment.length > 0 && doc.length > 0){
		$("#docs-list").append("<tr><td>"+comment+"</td><td>"+doc+"</td></tr>");
		$("#subject").val("");
		$("#showatt").html("");
	} 
	
	$("#modal-docs").modal('hide');
}

function fillModalMeaters(unitid)
{
	$.ajax({
	  type: "GET",
      cache: false,
      url: "/property/unitmeaters/" + unitid,
      success: function(unit) {
        $("#unit-title").html(unit.title);
		date = new Date();
		$("#unit-meaters").html("");
		var notset = true;
		for(var i=0; i<unit.meaters.length; i++)
		{
			d = new Date (unit.meaters[i].date);
			if(date.getFullYear() == d.getFullYear() && date.getMonth() == d.getMonth()) notset = false;
			$("#set-unit-meaters").attr("action","/property/setmeaters/"+unit._id);
			$("#unit-meaters").append("<tr id='"+ unit.meaters[i]._id +"' ><td>"+ d.getFullYear() + " " + lvMonths[d.getMonth()] +" rādījums</td><td class='cold' >"+unit.meaters[i].coldwater +"</td><td class='hot' >"+unit.meaters[i].hotwater+"</td><td class='heat' >"+unit.meaters[i].heat+"</td><td class='edit'><i class='icon-pencil' onclick='editMeater(\""+unit.meaters[i]._id+"\")'></i></td>");
		}
		if(notset)
		{
			$("#save").show();
			$("#set-unit-meaters").attr("action","/property/setmeaters/"+unit._id);
			$("#unit-meaters").append("<tr><td>"+ date.getFullYear()+" "+ lvMonths[date.getMonth()] + "</span> rādījums</td><td><input type='number' class='span2' name='cold' ></td><td><input type='number' class='span2' name='hot' ></td><td><input type='number' class='span2' name='heat' ></td><td><input type='hidden' name='mid' value='new' ></td></tr>");
		}
		else
		{
			$("#save").hide();
		}
		
      },
      error: function(unit) {           
        alert(":( Kaut kas nogāja greizi.");
      }
	});
}

function editMeater(mid)
{
	$("#save").show();
	var cold = $("#"+mid).children(".cold").html();
	var hot = $("#"+mid).children(".hot").html();
	var heat = $("#"+mid).children(".heat").html();
	$("#"+mid).children(".cold").html("<input type='number' class='span2' name='cold' value='"+cold+"'>");
	$("#"+mid).children(".hot").html("<input type='number' class='span2' name='hot' value='"+hot+"'>");
	$("#"+mid).children(".heat").html("<input type='number' class='span2' name='heat' value='"+heat+"'>");
	$("#"+mid).children(".edit").html("<input type='hidden' name='mid' value='"+mid+"' ><i class='icon-ban-circle' onclick='cancelMeaterEdit(\""+mid+"\")' ></i>");
}

function cancelMeaterEdit(mid)
{
	var cold = $("#"+mid).children(".cold").children().val();
	var hot = $("#"+mid).children(".hot").children().val();
	var heat = $("#"+mid).children(".heat").children().val();
	$("#"+mid).children(".cold").html(cold);
	$("#"+mid).children(".hot").html(hot);
	$("#"+mid).children(".heat").html(heat);
	$("#"+mid).children(".edit").html("<i class='icon-pencil' onclick='editMeater(\""+mid+"\")'></i>")
}
