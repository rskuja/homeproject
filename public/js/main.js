$(function(){
	// Style default buttons
	// TODO: Move this into an application js library
	$(".button-edit").button({ icons: {	primary: 'ui-icon-wrench' }	});
	$(".button-delete").button({ icons: { primary: 'ui-icon-trash' } });
	$(".button-home").button({ icons: {	primary: 'ui-icon-home'	} });
	$(".button-submit").button({ icons: { primary: 'ui-icon-disk' } });
	$(".button-cancel").button({ icons: {primary: 'ui-icon-close' } });

	// Date prettify
	prettyLinks();
	prettyMonths();
	$(".email").twipsy({placement: "below"});
	$(".alert-message").alert();

	$(".fcomment").click(function (){
    	$(this).parent().children('.uiUfi').removeClass('hidden'); 
    	$(this).parent().children('.uiUfi').children('.ufiItem').children().children().focus();
    });

    $(".addrecipient").click(function(){
        $('#modal-from-dom').modal('hide');
        var id = $(this).children(".toid").html();
        var addit = true;
        $(".ids").each(function(){
        	if($(this).val() == id)
        	{
        		alert("Šis lietotājs jau ir pievienots");
        		addit = false;
        	}
        });
        if(addit){
        $("#tofields").append('<span class="jm-MailSelectedRecipient"><input type="hidden" name="toid" class="ids" value="'+$(this).children(".toid").html()+'"><span class="toname">'+$(this).children(".name").html()+'</span><img  class="jm-MailSelectedRecipientX" src="/buttons/x12_b.png" alt="" onclick="$(this).parent().remove();" ></span>');
    	}
	});



	if($(".addrecipient").length == 0)
	{
		$("#modal-from-dom").children(".modal-header").children("h5").html("Jums nav kaimiņi vai apsaimniekotāji, kuriem nosūtīt vēstuli");
	}

	// sets up flexslider
	$('.flexslider').flexslider({
    	animation: "fade",              //String: Select your animation type, "fade" or "slide"
		slideDirection: "horizontal",   //String: Select the sliding direction, "horizontal" or "vertical"
		slideshow: true,                //Boolean: Animate slider automatically
		slideshowSpeed: 5000,           //Integer: Set the speed of the slideshow cycling, in milliseconds
		animationDuration: 600,
		directionNav: false,             //Boolean: Create navigation for previous/next navigation? (true/false)
		controlNav: false
    });

    $("a.grouped_elements").fancybox();

  	$(".commentBody").each(function(){
	    var that = $(this),
	      txt = that.html(),
	      options = {
	        callback: function( text, href ) {
	          return href ? '<a href="' + href + '" title="' + href + '">' + text + '</a>' : text;
	        }
	      };
	    
	    that.html(linkify( txt, options ));
	});


    $("p").each(function(){
	    var that = $(this),
	      txt = that.html(),
	      options = {
	        callback: function( text, href ) {
	          return href ? '<a href="' + href + '" title="' + href + '">' + text + '</a>' : text;
	        }
	      };
	    
	    that.html(linkify( txt, options ));
	});
	$(".jm-ListItem").click(function(){this.className='jm-ListTableSelected';});


	$("a[rel=popover]")
	.popover({
		offset: 10
	})
	.click(function(e) {
		e.preventDefault()
	});
	$("input[rel=popover]")
		.popover({
		  offset: 10,
		//trigger: 'focus'
		})
	.click(function(e) {
	  e.preventDefault()
	});

	$("th[rel=popover]")
		.popover({
		  offset: 10
		})
	.click(function(e) {
	  e.preventDefault()
	})

	});

      $.ajax({
        type: "GET",
        url: "/getuser",
        cache: false,
        success: function(msg) {
          if(msg != "false"){
	          for(var i=0; i<msg.user.houses.length; i++)
	          {
	            if(msg.house == msg.user.houses[i]._id){
	              $('#active-property').append('<img class="jm-LeftBar-ProfileImage"  src="/images/houses/'+msg.user.houses[i].picture+'" alt="">')
	              $('#activehouse-top').append('<a href="#" class="dropdown-toggle" ><img src="/images/houses/'+msg.user.houses[i].picture + '" class="tinyhouse" />'+msg.user.houses[i].name+'</a>');
	              $('#active-property').append('<span>'+msg.user.houses[i].name+'</span>');
	              var userid = msg.user._id;
	              $.ajax({
	                type: "GET",
	                cache: false,
	                url: "/houseinfo/" + msg.user.houses[i]._id,
	                success: function(house) {
	                  for(var k=0; k< house.units.length; k++){
	                    if(house.units[k].owner == userid) $('#active-property').append("<span>"+house.units[k].title+"</span>");
	                  }
	                }
	              });
	              break;
	            }
	          }
	          for(var i=0; i<msg.user.houses.length; i++)
	          {
	            if(msg.house != msg.user.houses[i]._id){
	            $('#inactivehouses-top').append('<li><a href="/property/switch/'+msg.user.houses[i]._id+'"  ><img src="/images/houses/'+msg.user.houses[i].picture + '" class="tinyhouse" />'+msg.user.houses[i].name+'</a></li>');
	            }
	          }
	          $('#inactivehouses-top').append('<li><a href="/property/new" ><img class="tinyhouse" src="/images/home_add.png" alt="new" />Pievienot Jaunu</a></li>');
	          if(msg.user.houses.length == 0){
	            $('.addhouse').append('<a href="/property/new" ><img class="tinyhouse" src="/images/home_add.png" alt="new" />Pievienot Jaunu</a>');
	          }
	          for(var i=0; i<msg.user.invites.length; i++)
	          {
	            var unit_id = msg.user.invites[i]._id;
	            var inv_id = msg.user.invites[i].parent;
	            var inv_title = msg.user.invites[i].title;
	            $.ajax({
	              type: "GET",
	              cache: false,
	              url: "/houseinfo/" + inv_id,
	              success: function(house) {
	                $('#important').append("<div class='well compact'><h3 class='red'>Jums ir ielūgums pievienoties mājai "+house.name+"</h3><p>Mājas īpašnieks jūs norādījis kā - "+ inv_title +"</p><button class='btn success' onclick=\"confirmhouse('"+unit_id+"');\" >Apstiprināt</button> </div>");
	              }
	            });
	            
	          }
          }
    	}
      }); 
      
      
	    $(".minicomment").keypress(function(e){
	      var dnd = false;
	      if(e.which == 13){
	        if(!dnd)
	        {
	          dnd = true;
	          $.ajax({
	          	 cache: false,
	             type: "POST",
	             url: "/talks/addcomment/" + this.id,
	             data: "comment[content]=" + this.value,
	             success: function(msg) {          
	              dnd = false;
	              location.reload();
	             }
	        });
	        }
	        return false;
	      }

	      //alert(this.id);
	    });


      HousePicUploader();
	  TalkPicUploader();
	  InboxPicUploader();
	  TalkAttUploader();
	  HouseAttUploader();
	  HouseDocUploader();



			




    