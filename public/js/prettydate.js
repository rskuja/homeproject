/*
 * JavaScript Pretty Date
 * Copyright (c) 2011 John Resig (ejohn.org)
 * Licensed under the MIT and GPL licenses.
 */

// Takes an ISO time and returns a string representing how
// long ago the date represents.
function prettyDate(time){
	var date = new Date((time || "").replace(/-/g,"/").replace(/[TZ]/g," ")),
		diff = (((new Date()).getTime() - date.getTime()) / 1000),
		day_diff = Math.floor(diff / 86400);
			
	if ( isNaN(day_diff) || day_diff < 0 || day_diff >= 31 )
		return "tikko";
			
	return day_diff == 0 && (
			diff < 60 && "tikko" ||
			diff < 120 && "1 minūti atpakaļ" ||
			diff < 3600 && Math.floor( diff / 60 ) + " minūtes atpakaļ" ||
			diff < 7200 && "pirms stundas" ||
			diff < 86400 && Math.floor( diff / 3600 ) + " stundas atpakaļ") ||
		day_diff == 1 && "vakar" ||
		day_diff < 7 && day_diff + " dienas atpakaļ" ||
		day_diff < 31 && Math.ceil( day_diff / 7 ) + " nedēļas atpakaļ";
}

// If jQuery is included in the page, adds a jQuery plugin to handle it as well
if ( typeof jQuery != "undefined" )
	jQuery.fn.prettyDate = function(){
		return this.each(function(){
			var date = prettyDate(this.title);
			if ( date )
				jQuery(this).text( date );
		});
	};
