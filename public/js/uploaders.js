function HousePicUploader(){    
  var housepicup = window.document.getElementById('homepic');
  if(housepicup != null)
  {        
    var uploader = new qq.FileUploader({
        element: housepicup,
        action: '/server/upload',
        allowedExtensions: ['jpg', 'png', 'gif', 'pdf'],
        multiple:true,
        params: {
          path: 'housepic'
        },
        onComplete: function(id, fileName, responseJSON){
          $(".qq-upload-success").remove();
          $("#showpic").html("");
          $("#showpic").append("<img src='/images/houses/"+responseJSON.name+"' class='mediumpic' />  ");
          $("#showpic").append("<input type='hidden' name='property[picture]' value='"+responseJSON.name+"' >");
        }
    });
  }           
} 

function TalkPicUploader(){    
  var talkpicup = window.document.getElementById('talkpic');
  if(talkpicup != null)
  {        
    var uploader = new qq.FileUploader({
        element: talkpicup,
        action: '/server/upload',
        allowedExtensions: ['jpg', 'png', 'gif'],
        multiple:true,
        params: {
          path: 'talkpic'
        },
        onComplete: function(id, fileName, responseJSON){
          $(".qq-upload-success").remove();
          $("#showpic").append("<img src='/images/talks/"+responseJSON.name+"' class='mediumpic' />  ");
          $("#showpic").append("<input type='hidden' name='pictures' value='"+responseJSON.name+"' >");
        }
    });
  }           
}

function InboxPicUploader(){    
  var inboxpicup = window.document.getElementById('inboxpic');
  if(inboxpicup != null)
  {        
    var uploader = new qq.FileUploader({
        element: inboxpicup,
        action: '/server/upload',
        allowedExtensions: ['jpg', 'png', 'gif'],
        multiple:true,
        params: {
          path: 'inboxpic'
        },
        onComplete: function(id, fileName, responseJSON){
          $(".qq-upload-success").remove();
          $("#showpic").append("<img src='/images/inbox/"+responseJSON.name+"' class='mediumpic' />  ");
          $("#showpic").append("<input type='hidden' name='pictures' value='"+responseJSON.name+"' >");
        }
    });
  }           
}


function TalkAttUploader(){    
  var talkatt = window.document.getElementById('talkatt');
  if(talkatt != null)
  {        
    var uploader = new qq.FileUploader({
        element: talkatt,
        action: '/server/upload',
        allowedExtensions: ['doc', 'docx', 'odt','xls', 'xlsx','ods', 'ppt', 'pptx', 'odp', 'pdf', 'txt','zip','rar'],
        multiple:true,
        params: {
          path: 'talkatt'
        },
        onComplete: function(id, fileName, responseJSON){
          $(".qq-upload-success").remove();
          var ext = (-1 !== responseJSON.name.indexOf('.')) ? responseJSON.name.replace(/.*[.]/, '').toLowerCase() : '';
          $("#showatt").append("<a href='/attachments/talks/"+responseJSON.name+"' ><img src='/buttons/ico-"+ext+".png' alt=''/></a>");
          $("#showatt").append("<input type='hidden' name='attachments' value='"+responseJSON.name+"' >");
        }
    });
  }           
}

function HouseAttUploader(){    
  var talkatt = window.document.getElementById('houseatt');
  if(talkatt != null)
  {        
    var uploader = new qq.FileUploader({
        element: talkatt,
        action: '/server/upload',
        allowedExtensions: ['doc', 'docx', 'odt','xls', 'xlsx','ods', 'ppt', 'pptx', 'odp', 'pdf', 'txt','zip','rar'],
        multiple:true,
        params: {
          path: 'houseatt'
        },
        onComplete: function(id, fileName, responseJSON){
          $(".qq-upload-success").remove();
          var ext = (-1 !== responseJSON.name.indexOf('.')) ? responseJSON.name.replace(/.*[.]/, '').toLowerCase() : '';
          $("#showatt").append("<a href='/attachments/houses/"+responseJSON.name+"' ><img src='/buttons/ico-"+ext+".png' alt=''/></a>");
          $("#showatt").append("<input type='hidden' name='attachments' value='"+responseJSON.name+"' >");
        }
    });
  }           
}



function HouseDocUploader(){    
  var talkatt = window.document.getElementById('housedoc');
  if(talkatt != null)
  {        
    var uploader = new qq.FileUploader({
        element: talkatt,
        action: '/server/upload',
        allowedExtensions: ['doc', 'docx', 'odt','xls', 'xlsx','ods', 'ppt', 'pptx', 'odp', 'pdf', 'txt','zip','rar'],
        multiple:true,
        params: {
          path: 'houseatt'
        },
        onComplete: function(id, fileName, responseJSON){
          $(".qq-upload-success").remove();
          $("#showatt").html("");
          var ext = (-1 !== responseJSON.name.indexOf('.')) ? responseJSON.name.replace(/.*[.]/, '').toLowerCase() : '';
          $("#showatt").append("<a href='/attachments/houses/"+responseJSON.name+"' ><img src='/buttons/ico-"+ext+".png' alt=''/></a>");
          $("#showatt").append("<input type='hidden' name='attachments' value='"+responseJSON.name+"' >");
        }
    });
  }           
}
