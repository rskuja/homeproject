var mongoose = require('mongoose'),	
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;


var Inbox = new Schema({
	from: { type: Schema.ObjectId, ref: 'User'},
	date: { type: Date, required: true},
	to: [{type: Schema.ObjectId, ref: 'User'}],
	subject: { type: String, required: false },
	body: { type: String, required: true },
	pictures: [{ type: String, required: false}]
});

mongoose.model('Inbox', Inbox);