var conf = require('./auth_conf');

var everyauth = require('everyauth')
  , Promise = everyauth.Promise;
var Units = require('../models/Units');
everyauth.debug = true;
var mongoose = require('mongoose')
  , Units = mongoose.model('Units')
  , Schema = mongoose.Schema
  , ObjectId = mongoose.SchemaTypes.ObjectId;

var UserSchema = new Schema({
  houses: [{ type: Schema.ObjectId, ref: 'Property', required: false}],
  invites: [{ type: Schema.ObjectId, ref: 'Units', required: false}],
  units: [{ type: Schema.ObjectId, ref: 'Units', required: false}],
  inbox: [{ type: Schema.ObjectId, ref: 'Inbox', required: false}]
})
  , User;


var mongooseAuth = require('mongoose-auth');

UserSchema.plugin(mongooseAuth, {
    everymodule: {
      everyauth: {
          User: function () {
            return User;
          }
      }
    }
  , facebook: {
      everyauth: {
          myHostname: 'http://dev.skuja.lv:3000'
        , appId: conf.fb.appId
        , appSecret: conf.fb.appSecret
        , redirectPath: '/'
        , findOrCreateUser: function (session, accessTok, accessTokExtra, fbUser) {
              var promise = this.Promise()
                  , User = this.User()();
              if(session.auth)
              {
                  User.findById(session.auth.userId, function (err, user) {
                    if (err) return promise.fail(err);
                    if (!user) {
                      User.where('password.login', fbUser.email).findOne( function (err, user) {
                        if (err) return promise.fail(err);
                        if (!user) {
                          User.createWithFB(fbUser, accessTok, accessTokExtra.expires, function (err, createdUser) {
                            if (err) return promise.fail(err);
                            return promise.fulfill(createdUser);
                          });
                        } else {
                          assignFbDataToUser(user, accessTok, accessTokExtra, fbUser);
                          user.save( function (err, user) {
                            if (err) return promise.fail(err);
                            promise.fulfill(user);
                          });
                        }
                      });
                    } else {
                      assignFbDataToUser(user, accessTok, accessTokExtra, fbUser);

                      // Save the new data to the user doc in the db
                      user.save( function (err, user) {
                        if (err) return promise.fail(err);
                        promise.fuilfill(user);
                      });
                    }
                  });
                  
             }
             else
             {
                  User.findOne({'fb.id': fbUser.id}, function (err, foundUser) {
                  if (foundUser) {
                    return promise.fulfill(foundUser);
                  }
                  console.log("CREATING USER");
                  User.createWithFB(fbUser, accessTok, accessTokExtra.expires, function (err, createdUser) {
                    if (err) return promise.fail(err);
                    return promise.fulfill(createdUser);
                  });
                });
             }
             return promise; // Make sure to return the promise that promises the user
            }
      }
  }
    
  , twitter: {
      everyauth: {
          myHostname: 'http://dev.skuja.lv:3000'
        , consumerKey: conf.twit.consumerKey
        , consumerSecret: conf.twit.consumerSecret
        , redirectPath: '/'
      }
    }
  , password: {
        loginWith: 'email'
      , extraParams: {
          fullname: String,
          houses: [{ type: Schema.ObjectId, ref: 'Property', required: false }],
          invites: [{ type: Schema.ObjectId, ref: 'Units', required: false}],
          units: [{ type: Schema.ObjectId, ref: 'Units', required: false}],
          inbox: [{ type: Schema.ObjectId, ref: 'Inbox', required: false}]
        }
      , everyauth: {
            getLoginPath: ' '
          , postLoginPath: '/login'
          , loginView: './app/index.html'
          , getRegisterPath: ' '
          , postRegisterPath: '/register'
          , registerView: './app/index.html'
          , loginSuccessRedirect: '/'
          , registerSuccessRedirect: '/'
          , respondToRegistrationSucceed: function (res, user, data) {
            if(user.units.length != 0){
              Units.findById(user.units[0], function(err, unit){
                unit.owner = user.id;
                unit.save();
                res.redirect("/");
              });
            }
            else res.redirect("/");
          }
          
        }
    }
});

// Assign all properties - see lib/modules/facebook/schema.js for details
function assignFbDataToUser (user, accessTok, accessTokExtra, fbUser) {
  user.fb.accessToken = accessTok;
  user.fb.expires = accessTokExtra.expires;
  user.fb.id = fbUser.id;
  user.fb.name.first = fbUser.first_name;
  // etc. more assigning...
}

mongoose.model('User', UserSchema);

User = mongoose.model('User');