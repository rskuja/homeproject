var mongoose = require('mongoose'),	
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var Units = new Schema({
	parent: { type: Schema.ObjectId, ref: 'Property'},
	title: { type: String, required: false},
	name: { type: String, required: false},
	surname: { type: String, required: false},
	email: { type: String, required: true},
	phone: { type: String, required: false},
	owner: { type: Schema.ObjectId, ref: 'User'},
	meaters: [{ type: Schema.ObjectId, ref: 'Meaters'}],
	usermeaters: [{ type: String, required: false}]
});

var Meaters = new Schema({
	unit: { type: Schema.ObjectId, ref: 'Units'},
	date: { type: Date, required: true},
	hotwater: { type: Number, required: false},
	coldwater: { type: Number, required: false},
	heat: { type: Number, required: false}
});

mongoose.model('Units',Units);
mongoose.model('Meaters',Meaters);