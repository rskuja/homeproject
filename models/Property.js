/**
 *  Property schema
 **/

var mongoose = require('mongoose'),	
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var Property = new Schema({
	admins: [{ type: Schema.ObjectId, ref: 'User' }],
	talks: [{type: Schema.ObjectId, ref: 'Talk'}],
	units: [{type: Schema.ObjectId, ref: 'Units'}],
	name: { type: String, required: true},
	creation_date: { type: Date, required: true},
	street: { type: String, required: true},
	homenr: { type: String, required: true},
	city: { type: String, required: false},
	region: { type: String, required: false},
	postal_code: { type: String, required: false},
	country: { type: String, required: true},
	unitcount: { type: Number, required: false},
	picture: { type: String, required: false},
	decisions: [{ type: Schema.ObjectId, ref: 'PropertyDecision'}],
	plannedworks:[{type: Schema.ObjectId, ref: 'Works'}],
	rulles: { type: String, required: false},
	personal: { type: [Personal], required: false},
	parkings: { type: [ParkingPlace], required: false}
});

var ParkingPlace = new Schema ({
	title: {type: String, required: true},
	unit: {type: Schema.ObjectId, ref: 'Units', required: false}
});

var Personal = new Schema({
	title: {type: String, required: true},
	fullname: {type: String, required: true},
	email: {type: String, required: false},
	phone: {type: String, required: false}
});

var PropertyDecision = new Schema({
	property: {type: Schema.ObjectId, ref: 'Property'},
	subject: {type: String, required: true},
	attachments: [{type: String, required: false}],
	date: {type: Date, required: true},
	manager: { type: Schema.ObjectId, ref: 'User' },
	ontalk: {type: Schema.ObjectId, ref: 'Talk'},
	canceled: {type: Boolean, required: false},
	cancel_date: {type: Date, required: false}
});

var Works = new Schema({
	property: {type: Schema.ObjectId, ref:'Property'},
	comment: {type: String, required: true},
	date: {type: Date, required: true},
	creator: {type: Schema.ObjectId, ref: 'User'},
	canceled: {type:  Boolean, required: false},
	cancel_date: {type: Date, required: false}
});

mongoose.model('Property', Property);
mongoose.model('PropertyDecision', PropertyDecision);
mongoose.model('Works', Works);
mongoose.model('Personal', Personal);
mongoose.model('ParkingPlace', ParkingPlace);