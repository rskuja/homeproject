var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var VoteOption = new Schema({
	parent : { type: Schema.ObjectId, ref: 'Talk' },
	option: {type: String, required: true },
	allowchange: {type: Boolean, required: false},
	allowmultiple: {type: Boolean, required: false},
	count: { type: Number, required: true}
});

mongoose.model('VoteOption', VoteOption);