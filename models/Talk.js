/**
 *  Talk schema
 *  Created by create-model script  
 **/
 
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var Talk = new Schema({
	  title:{type: String, required: true},
	  house: { type: Schema.ObjectId, ref: 'Property' },
	  content: {type: String, required: false},
	  userid: { type: String, required: true},
	  username: { type: String, required: true},
	  date: { type: Date, required: true},
	  comments: { type: [Comments], required: false},
	  votes : [{ type: Schema.ObjectId, ref: 'VoteOption' }],
	  uservotes : [{ type: Schema.ObjectId, ref: 'UserVote' }],
	  pictures: [{ type: String, required: false}],
	  attachments: [{ type: String, required: false}],
	  decision: { type: Schema.ObjectId, ref: 'PropertyDecision'}
});

var UserVote = new Schema({
	parent : { type: Schema.ObjectId, ref: 'Talk'},
	user : { type: String, required: true},
	vote : { type: Schema.ObjectId, ref: 'VoteOption'}
});

var Comments = new Schema({
	content:{ type: String, required: true},
	date:{ type: Date, required: true},
	userid: { type: String, required: true},
	username: { type: String, required: true}
});

mongoose.model('Comments', Comments);
mongoose.model('Talk', Talk);
mongoose.model('UserVote', UserVote);